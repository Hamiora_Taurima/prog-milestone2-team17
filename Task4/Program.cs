﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rate_Fav_food
{
    class Program
    {
        static Dictionary<string, string> vegList;


        public static void addfood()
        {
            int i;

            vegList = new Dictionary<string, string>();
            for (i = 1; i <= 5; i++)
            {
                Console.WriteLine("Enter Food " + i);
                string food = Console.ReadLine();
                Console.WriteLine("Rate for Food " + i + " (1-5)");

                string rate = Console.ReadLine();
                int rate_value;


                if (int.TryParse(rate, out rate_value))
                {
                    vegList.Add(food, rate);


                }
                else
                {
                    Console.WriteLine("Rate Should be an integer!");
                    i--;

                }


            }

        }



        public static void showfood()
        {
            int v1, j;


            for (j = 1; j <= 5; j++)
            {
                foreach (KeyValuePair<string, string> veg1 in vegList)
                {
                    if (int.TryParse(veg1.Value, out v1))
                    {

                        if (v1 == j)
                        {
                            Console.WriteLine("Food = {0}, Rate = {1}", veg1.Key, veg1.Value);
                        }
                    }
                }


            }

        }


        public static void changerate()
        {
            int v1, j;



            foreach (KeyValuePair<string, string> veg1 in vegList)
            {
                if (int.TryParse(veg1.Value, out v1))
                {


                    Console.WriteLine("Food = {0}, Rate = {1}", veg1.Key, veg1.Value);
                    Console.WriteLine("Change Rate Value (1 - 5), Zero(0) for no chnage :");
                    string x = veg1.Value;

                    string rate = Console.ReadLine();
                    if (rate != "0")
                    {
                        vegList[x] = rate;
                    }



                }
            }



        }




        static void Main(string[] args)
        {
            int ch;

            do
            {


                Console.WriteLine("1. Add Foods");
                Console.WriteLine("2. Show Foods");
                Console.WriteLine("3. Change Rate of Food");
                Console.WriteLine("4. Exit");
                Console.WriteLine("Enter your Choice");
                ch = Convert.ToInt32(Console.ReadLine());



                switch (ch)
                {
                    case 1:
                        rate_Fav_food.Program.addfood();
                        break;
                    case 2:
                        rate_Fav_food.Program.showfood();
                        break;

                    case 3:

                        rate_Fav_food.Program.changerate();

                        break;
                    case 4:
                        Console.WriteLine("Bye !!!!!");
                        break;
                }


            } while (ch != 4);

            Console.ReadLine();




        }
    }
}

