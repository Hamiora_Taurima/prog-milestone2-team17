﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = "m";



            do
            {

                Console.Clear();                
                Console.WriteLine("######################################");
                Console.WriteLine("#  ________________________________  #");
                Console.WriteLine("# |                                | #");
                Console.WriteLine("# |         Team 17 Menu           | #");
                Console.WriteLine("# |________________________________| #");
                Console.WriteLine("#                                    #");
                Console.WriteLine("#                                    #");               
                Console.WriteLine("#   1. Date Calculator          (1)  #");
                Console.WriteLine("#   2. Grade Average Calculator (2)  #");
                Console.WriteLine("#   3. Random Number Calculator (3)  #");
                Console.WriteLine("#   4. Favorite Food Rating     (4)  #");
                Console.WriteLine("#                                    #");
                Console.WriteLine("#   Please select an option :        #");
                Console.WriteLine("######################################");
                menu = Console.ReadLine();



                if (menu == "1")
                {
                    var selection = "Q";
                    do
                    {
                        Console.Clear();
                        Console.WriteLine("*       Date Calculator Menu         *");
                        
                        Console.WriteLine("     1. Days old Calculator      (1) ");
                        Console.WriteLine("     2. Days in Years Calculator (2) ");
                        Console.WriteLine("                                     ");
                        Console.WriteLine("      Please select an option :       ");

                        selection = Console.ReadLine();

                        if (selection == "1")
                        {
                            Console.Clear();
                            Console.WriteLine("");

                           
                            Console.WriteLine("  Press m to return to the main menu");
                            menu = Console.ReadLine();
                        }
                        else if (selection == "2")
                        {
                            Console.Clear();
                            Console.WriteLine("");

                          
                            Console.WriteLine("  Press m to return to the main menu");
                            menu = Console.ReadLine();
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine(" ");
                            Console.WriteLine("     Incorrect input  ");
                            Console.WriteLine("");
                            Console.ReadLine();
                            Console.WriteLine("  Press m to return to the main menu");
                            menu = Console.ReadLine();
                        }
                    } while (selection == "Q");


                    Console.Clear();
                    Console.WriteLine("");
                    Console.WriteLine("  Press m to return to the main menu");
                    menu = Console.ReadLine();
                }
                else if (menu == "2")
                {
                    Console.Clear();
                    Console.WriteLine("");
                    Console.WriteLine("  Press m to return to the main menu");
                    menu = Console.ReadLine();
                }

                else if (menu == "3")
                {
                    Console.Clear();
                    Console.WriteLine("");
                    Console.WriteLine("  Press m to return to the main menu");
                    menu = Console.ReadLine();
                }
                else if (menu == "4")
                {
                    Console.Clear();
                    Console.WriteLine("");
                    Console.WriteLine("  Press m to return to the main menu");
                    menu = Console.ReadLine();
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine(" ");
                    Console.WriteLine("            Incorrect input  ");
                    Console.WriteLine("");
                    Console.ReadLine();
                    Console.WriteLine("  Press m to return to the main menu");
                    menu = Console.ReadLine();

                }
            } while (menu == "m");
        }
    }
}
