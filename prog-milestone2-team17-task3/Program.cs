﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team17_task3
{
    class Program
    {
        static void Main(string[] args)
        {
            

                int roundcounter = 0;

                int score = 0;

                int totalgamesplayed = 0;

                int round = 0;

                bool repeat = true;

            while (repeat) //**** While loop to repeat game once finished ****//
            {

                //**** Starting message and totalgamesplayed +1 at the begining of game code ****//
                totalgamesplayed++;

                Console.WriteLine("Hello!");
                Console.WriteLine();
                Console.WriteLine("In this game you have to guess what number is going to appear 1-5.");
                Console.WriteLine();
                Console.WriteLine("Good luck!");
                Console.WriteLine();
                Console.WriteLine("Press any key to begin");
                Console.WriteLine();
                Console.ReadKey();
                Console.Clear();

               
                do
                {
                    round++;

                    Console.WriteLine($"Round {round}. Guess a number between 1 and 5.");
                    Console.WriteLine();

                    Random rnd = new Random();
                    int ranvalue = rnd.Next(1, 5); //**** Random value between 1 and 5 ****//

                    int userguess = Int32.Parse(Console.ReadLine()); //**** User guess added to int userguess ****//

                    Console.WriteLine();
                    Console.WriteLine($"The number to guess is {ranvalue}!"); //**** random number set for userguess int to be compared to ****//

                    

                    if (userguess == ranvalue) //**** if statement to compare userguess value to ranvalue value ****//
                    {
                        score++; //**** If correct adds 1 point to user score via int score ****//

                        Console.WriteLine();
                        Console.WriteLine($"You guessed correctly! your score is now {score}");
                        Console.WriteLine();

                        roundcounter++;
                    }

                    else //**** Otherwise provides an incorrect message to user ****//
                    {
                        Console.WriteLine();
                        Console.WriteLine("Incorrect!");
                        Console.WriteLine();

                        roundcounter++;
                    }

                } while (roundcounter < 5); //**** Max number of rounds set to 5 ****//
                {
                    //**** Asks user if they want to play again or exit ****//
                    Console.Clear();
                    Console.WriteLine();
                    Console.WriteLine($"Thanks for playing! Your final score was {score}.");
                    Console.WriteLine();
                    Console.WriteLine("Play again... ?");
                    Console.WriteLine();
                    Console.WriteLine("Y / N");
                    Console.WriteLine();

                    var again = Console.ReadLine();

                    //**** If statement dependant on user input ****//
                    if (again == "y")
                    {
                        score = 0;
                        roundcounter = 0; //**** Resets previous game values for next game ****//
                        round = 0;

                        Console.Clear();
                    }

                    else
                    {
                        //**** Exit message, score and total games user has played displayed****//

                        Console.Clear();
                        Console.WriteLine($"Your final score was {score}, and you've played a total of {totalgamesplayed} games!");
                        Console.WriteLine();
                        Console.WriteLine("Thanks for playing! Press enter to exit.");
                        Console.WriteLine();
                        Environment.Exit(0);
                    }
                }

            }

            }
        }
    }
